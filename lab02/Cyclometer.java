///// CSE02 YUEHAN WANG/////
/////      2/1/2019    /////
/////       lab02      /////
////////////////////////////
//MPG means "Miles Per Gallon", which is created to help calculate the total miles with certain amount of gallon used.//
public class Cyclometer {
  //main method for java program
  public static void main(String arg[]){

  int secsTrip1=480; //time (in seconds) used during 1st trip
  int secsTrip2=3220; //time (in seconds) used during 2nd trip
  int countsTrip1=1561; //total rounds rotated by the wheel during 1st trip
  int countsTrip2=9037; //total rounds rotated by the wheel during 2nd trip
    
  double wheelDiameter=27.0, //diameter of the wheel, for calculating the length the wheel traveled in one rotation
  PI=3.14159, //for calculating the length the wheel traveled in one rotation
  feetPerMile=5280, //for transfering feet to miles
  inchesPerFoot=12, //for transfering inches to feet.
  secondsPerMinute=60; //for transfering seconds to minutes.
  double distanceTrip1, distanceTrip2, totalDistance; //define the three variables
  
    //print out the data
    System.out.println("Trip 1 took "+
                      (secsTrip1/secondsPerMinute)+ //trasfering the time 1st trip took into minutes
                      " minutes and had "+
                      countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
                      (secsTrip2/secondsPerMinute)+ //trasfering the time 2nd trip took into minutes
                       " minutes and had "+
                       countsTrip2+" counts.");
  distanceTrip1=countsTrip1*wheelDiameter*PI; //this is the distance the wheel traveled by rotating once (in inches)
  distanceTrip1/=inchesPerFoot*feetPerMile; //this transfers the value above from inches to miles
  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //this is the same with distanceTrip1
  totalDistance=distanceTrip1+distanceTrip2; //this calculates the total distance of trip1 and trip2
    
  //print out the data calculated
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
  System.out.println("Trip 2 was "+distanceTrip2+" miles");
  System.out.println("The total distance was "+totalDistance+" miles");
  
    
  }//end of main method
}//end of class
 