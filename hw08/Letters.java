import java.util.Random;  //inport the necessary utilities
import java.util.Arrays;
public class Letters{  //define the class
public static void main(String arg[]){  //main method
  
  int sizeOfArray = (int)(Math.random() * 21+5);  //randomly decide the size of array
  char[] letterArray = new char[sizeOfArray];  //create an array
  int randomNum = 0;
  char c = 'a';
   Random r = new Random();
  System.out.print("Random character array: ");
 for(int a=0; a<sizeOfArray; a++){  //generate an array with random letters
   randomNum = (int) (Math.random()*52+1);
    letterArray[a]=randomChar(randomNum);
   System.out.print(letterArray[a]);
 }
 System.out.println(" "); 
  
 System.out.print("AtoM characters: ");
  char[] AtoMArray = new char[letterArray.length];  //create a new array with only letters from A to M
  AtoMArray=getAtoM(letterArray);  
 for(int a=0; a<AtoMArray.length;a++){  //print out the sorted value
   System.out.print(AtoMArray[a]);
 }
 System.out.println(" "); 
  
 System.out.print("NtoZ characters: ");
  char[] NtoZArray = new char[letterArray.length]; //create a new array with only letters from A to M
  NtoZArray=getNtoZ(letterArray);
 for(int a=0; a<AtoMArray.length;a++){  //print out the sorted value
   System.out.print(NtoZArray[a]);
 }
 System.out.println(" "); 
  }  //end of main method
  
public static char randomChar(int a){  //random letter generator
  char c = 'a';
  switch(a){
    case 1: c='a'; break; case 2: c='b'; break; case 3: c='c'; break; case 4: c='d'; break; case 5: c='e'; break; case 6: c='f'; break;
    case 7: c='g'; break; case 8: c='h'; break; case 9: c='i'; break; case 10: c='j'; break; case 11: c='k'; break; case 12: c='l'; break;
    case 13: c='m'; break; case 14: c='n'; break; case 15: c='o'; break; case 16: c='p'; break; case 17: c='q'; break; case 18: c='r'; break;
    case 19: c='s'; break; case 20: c='t'; break; case 21: c='u'; break; case 22: c='v'; break; case 23: c='w'; break; case 24: c='x'; break;
    case 25: c='y'; break; case 26: c='z'; break; case 27: c='A'; break; case 28: c='B'; break; case 29: c='C'; break; case 30: c='D'; break;
    case 31: c='E'; break; case 32: c='F'; break; case 33: c='G'; break; case 34: c='H'; break; case 35: c='I'; break; case 36: c='J'; break;
    case 37: c='K'; break; case 38: c='L'; break; case 39: c='M'; break; case 40: c='N'; break; case 41: c='O'; break; case 42: c='P'; break;
    case 43: c='Q'; break; case 44: c='R'; break; case 45: c='S'; break; case 46: c='T'; break; case 47: c='U'; break; case 48: c='V'; break;
    case 49: c='W'; break; case 50: c='X'; break; case 51: c='Y'; break; case 52: c='Z'; break;
  }
  return c;  //return the letter
}

  //sorting the letters
public static char[] getAtoM(char[] letterArray){
  char[] AtoMArray = new char[letterArray.length];
  for(int a=0; a<letterArray.length; a++){  //only put the value from a to m into the new array, leave other alone
    if(letterArray[a]>='a'&&letterArray[a]<='m'){
      AtoMArray[a]=letterArray[a];
    }
    else if(letterArray[a]>='A'&&letterArray[a]<='M'){
      AtoMArray[a]=letterArray[a];
    }
  }
     return AtoMArray;
}

  //sorting the letters  
 public static char[] getNtoZ(char[] letterArray){
  char[] NtoZArray = new char[letterArray.length];
  for(int a=0; a<letterArray.length; a++){  //the same with last one with a range from n to z
    if(letterArray[a]>='n'&&letterArray[a]<='z'){
      NtoZArray[a]=letterArray[a];
    }
    else if(letterArray[a]>='N'&&letterArray[a]<='Z'){
      NtoZArray[a]=letterArray[a];
    }
  }
     return NtoZArray;
}
   
  
} //end of class