///// YUEHAN WANG CSE 02 /////
/////  HW04     2/19/19  /////
//////////////////////////////
//////////////////////////////


public class PokerHandCheck{   //define the class
  public static void main(String arg[]){   //main method of java program
    //------------------the first card------------------------------
    int theNumber1 = (int)(Math.random() * 52 + 1);  //create a random number from 1 to 52
    boolean smallNumber, Jack, Queen, King, Diamonds, Clubs, Hearts, Spades;   //define each variable
    Diamonds = Clubs = Hearts = Spades = Jack = Queen = King = smallNumber = false;   //initilize each variable
    switch (theNumber1){   //define the card type by its number
         case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13:
        Diamonds = true;
        break;
         case 14: case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26:
        Clubs = true;
        break;
         case 27: case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35: case 36: case 37: case 38: case 39:
        Hearts = true;
        break;
        case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49: case 50: case 51: case 52:
        Spades = true;
        break;
    }
     
    String suitDisplay = "";  //define and initilize the variable
    if (Diamonds){   //type in the print value
      suitDisplay = "Diamonds";
    }
    else if (Clubs){
      suitDisplay = "Clubs";
    }
    else if (Hearts){
      suitDisplay = "Hearts";
    }
    else if (Spades){
      suitDisplay = "Spades";
    }
 
    
    int theIntNumber1 = theNumber1 % 13;   //define whether it is bigger than 10, and make it an appropriate expression
    switch (theIntNumber1){
        case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10:
        smallNumber = true;
        break;
        case 11:
        Jack = true;
        break;
        case 12:
        Queen = true;
        break;
      case 0 :
        theIntNumber1 = 13;   //  because 13/26/39/52 % 13 makes it a 0, we need to change it to 13
        King = true;
        break;       
    }
   
    
    String wordDisplay = "";   //type in the print value
    if (Jack){
      wordDisplay = "Jack";
    }
    else if (Queen){   
      wordDisplay = "Queen";
    }  
    else if (King){
      wordDisplay = "King";
    }     
//--------------print first number------------------------------------------------------------
    System.out.println("Your random cards were: ");  //the first line instruction
        if (smallNumber){   //check which one to print out
    System.out.println("the " + theIntNumber1 + " of " + suitDisplay);   //print out the number and card type
     }
      else
    System.out.println("the " + wordDisplay + " of " + suitDisplay);
//---------------------------------------------------------------------------------------------------
                            //!!!!!!1the same steps for the rest four cards!!!!!
//--------------the second card----------------------------------------------------------------------
    int theNumber2 = (int)(Math.random() * 52 + 1);  //create a random number from 1 to 52
    Diamonds = Clubs = Hearts = Spades = Jack = Queen = King = smallNumber = false;
    switch (theNumber2){
         case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13:
        Diamonds = true;
        break;
         case 14: case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26:
        Clubs = true;
        break;
         case 27: case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35: case 36: case 37: case 38: case 39:
        Hearts = true;
        break;
        case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49: case 50: case 51: case 52:
        Spades = true;
        break;
    }
     
    if (Diamonds){
      suitDisplay = "Diamonds";
    }
    else if (Clubs){
      suitDisplay = "Clubs";
    }
    else if (Hearts){
      suitDisplay = "Hearts";
    }
    else if (Spades){
      suitDisplay = "Spades";
    }
    
    int theIntNumber2 = theNumber2 % 13;
    switch (theIntNumber2){
        case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10:
        smallNumber = true;
        break;
        case 11:
        Jack = true;
        break;
        case 12:
        Queen = true;
        break;
      case 0 :
        theIntNumber2 = 13;
        King = true;
        break;       
    }
   
    
    if (Jack){
      wordDisplay = "Jack";
    }
    else if (Queen){   
      wordDisplay = "Queen";
    }  
    else if (King){
      wordDisplay = "King";
    }  
//------------------------------print the second number---------------------------------------------------
        if (smallNumber){
    System.out.println("the " + theIntNumber2 + " of " + suitDisplay);
     }
      else
    System.out.println("the " + wordDisplay + " of " + suitDisplay);
//--------------the third card----------------------------------------------------------------------
    int theNumber3 = (int)(Math.random() * 52 + 1);  //create a random number from 1 to 52
    Diamonds = Clubs = Hearts = Spades = Jack = Queen = King = smallNumber = false;
    switch (theNumber3){
         case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13:
        Diamonds = true;
        break;
         case 14: case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26:
        Clubs = true;
        break;
         case 27: case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35: case 36: case 37: case 38: case 39:
        Hearts = true;
        break;
        case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49: case 50: case 51: case 52:
        Spades = true;
        break;
    }
     
    if (Diamonds){
      suitDisplay = "Diamonds";
    }
    else if (Clubs){
      suitDisplay = "Clubs";
    }
    else if (Hearts){
      suitDisplay = "Hearts";
    }
    else if (Spades){
      suitDisplay = "Spades";
    }
    
    int theIntNumber3 = theNumber3 % 13;
    switch (theIntNumber3){
        case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10:
        smallNumber = true;
        break;
        case 11:
        Jack = true;
        break;
        case 12:
        Queen = true;
        break;
      case 0 :
        theIntNumber3 = 13;
        King = true;
        break;       
    }
   
    
    if (Jack){
      wordDisplay = "Jack";
    }
    else if (Queen){   
      wordDisplay = "Queen";
    }  
    else if (King){
      wordDisplay = "King";
    }  
//------------------------------print the third number---------------------------------------------------
        if (smallNumber){
    System.out.println("the " + theIntNumber3 + " of " + suitDisplay);
     }
      else
    System.out.println("the " + wordDisplay + " of " + suitDisplay);
//--------------the fourth card----------------------------------------------------------------------
    int theNumber4 = (int)(Math.random() * 52 + 1);  //create a random number from 1 to 52
    Diamonds = Clubs = Hearts = Spades = Jack = Queen = King = smallNumber = false;
    switch (theNumber4){
         case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13:
        Diamonds = true;
        break;
         case 14: case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26:
        Clubs = true;
        break;
         case 27: case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35: case 36: case 37: case 38: case 39:
        Hearts = true;
        break;
        case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49: case 50: case 51: case 52:
        Spades = true;
        break;
    }
     
    if (Diamonds){
      suitDisplay = "Diamonds";
    }
    else if (Clubs){
      suitDisplay = "Clubs";
    }
    else if (Hearts){
      suitDisplay = "Hearts";
    }
    else if (Spades){
      suitDisplay = "Spades";
    }
    
    int theIntNumber4 = theNumber4 % 13;
    switch (theIntNumber4){
        case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10:
        smallNumber = true;
        break;
        case 11:
        Jack = true;
        break;
        case 12:
        Queen = true;
        break;
      case 0 :
        theIntNumber4 = 13;
        King = true;
        break;       
    }
   
    
    if (Jack){
      wordDisplay = "Jack";
    }
    else if (Queen){   
      wordDisplay = "Queen";
    }  
    else if (King){
      wordDisplay = "King";
    }  
//------------------------------print the fourth number---------------------------------------------------
        if (smallNumber){
    System.out.println("the " + theIntNumber4 + " of " + suitDisplay);
     }
      else
    System.out.println("the " + wordDisplay + " of " + suitDisplay);
//--------------the fifth card----------------------------------------------------------------------
    int theNumber5 = (int)(Math.random() * 52 + 1);  //create a random number from 1 to 52
    Diamonds = Clubs = Hearts = Spades = Jack = Queen = King = smallNumber = false;
    switch (theNumber5){
         case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13:
        Diamonds = true;
        break;
         case 14: case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26:
        Clubs = true;
        break;
         case 27: case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35: case 36: case 37: case 38: case 39:
        Hearts = true;
        break;
        case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49: case 50: case 51: case 52:
        Spades = true;
        break;
    }
     
    if (Diamonds){
      suitDisplay = "Diamonds";
    }
    else if (Clubs){
      suitDisplay = "Clubs";
    }
    else if (Hearts){
      suitDisplay = "Hearts";
    }
    else if (Spades){
      suitDisplay = "Spades";
    }
    
    int theIntNumber5 = theNumber5 % 13;
    switch (theIntNumber5){
        case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10:
        smallNumber = true;
        break;
        case 11:
        Jack = true;
        break;
        case 12:
        Queen = true;
        break;
      case 0 :
        theIntNumber5 = 13;
        King = true;
        break;       
    }
   
    
    if (Jack){
      wordDisplay = "Jack";
    }
    else if (Queen){   
      wordDisplay = "Queen";
    }  
    else if (King){
      wordDisplay = "King";
    }  
//------------------------------print the fifth number---------------------------------------------------
        if (smallNumber){
    System.out.println("the " + theIntNumber5 + " of " + suitDisplay);
     }
      else
    System.out.println("the " + wordDisplay + " of " + suitDisplay); 
//---------------------------------------------------------------------------------------------------
//----------------------------pairing the cards------------------------------------------------------
//---------------------------------------------------------------------------------------------------
    boolean threeOfaKind = false;  //define and initilize the varaible
    int pairs = 0;  //define and initilize the varaible
    if (theIntNumber1 == theIntNumber2){   //checking if there are three same cards, if not, increase the number of pairs by one
      pairs ++;
      if (theIntNumber2 == theIntNumber3){
        threeOfaKind = true;
      }
      else if (theIntNumber2 == theIntNumber4){
        threeOfaKind = true;
      }
      else if (theIntNumber2 == theIntNumber5){
        threeOfaKind = true;
      }
    }  
    else if (theIntNumber1 == theIntNumber3){   //sequence from number 1 to 4
       pairs ++;
      if (theIntNumber3 == theIntNumber4){
        threeOfaKind = true;
      }
      else if (theIntNumber3 == theIntNumber5){
        threeOfaKind = true;
      }
    }
    else if (theIntNumber1 == theIntNumber4){
       pairs ++;
      if (theIntNumber4 == theIntNumber5){
        threeOfaKind = true;
      }
    }
    else if (theIntNumber2 == theIntNumber3){
       pairs ++;
      if (theIntNumber3 == theIntNumber4){
        threeOfaKind = true;
      }
      else if (theIntNumber3 == theIntNumber5){
        threeOfaKind = true;
      }
    }
    else if (theIntNumber2 == theIntNumber4){
      pairs ++;
      if (theIntNumber4 == theIntNumber5){
        threeOfaKind = true;
      }
    }
    else if (theIntNumber3 == theIntNumber4){
       pairs ++;
      if (theIntNumber4 == theIntNumber5){        
      }
    } 
    
 //fill the rest 4 possible pairs that could not write into the code above 
    if (theIntNumber1 == theIntNumber5){
       pairs ++;
    }
    if (theIntNumber2 == theIntNumber5){
        pairs ++;
    }
    if (theIntNumber3 == theIntNumber5){
       pairs ++;
    }
    if (theIntNumber4 == theIntNumber5){
       pairs ++;
    }
    
//print the number. If there are three of a kind, print it first. Otherwise, count the number of pairs
    
    if (threeOfaKind){
      System.out.println("You have three of a kind!");
    }
    else if (pairs > 0){   //in case there is at least one pair
      System.out.println("You have " + pairs + " pairs!");
    }
    else System.out.println("You have a high card hand!");   //other cases that don't have any same numbers
     
    
  }  //end of class
}  //end of main method