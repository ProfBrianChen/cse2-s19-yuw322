import java.util.Scanner;
public class PatternD{
  public static void main(String arg[]){
    
    Scanner anInteger = new Scanner(System.in);
    
    boolean indicator = anInteger.hasNextInt();
    int integer = 0;
    if (indicator){
      integer = anInteger.nextInt();
    }
    else 
      anInteger.next();
    while(!indicator || integer>10 || integer<1 ){
      System.out.println("You entered an invalid number. Please try again.");
        indicator = anInteger.hasNextInt();
      while(!indicator){
        System.out.println("You entered an invalid number. Please try again.");
        anInteger.next();
        indicator = anInteger.hasNextInt();
      }
      integer = anInteger.nextInt();
      if(integer<10 && integer>0){
        break;
      } 
    }
    int a = 0;
    int b = 0;
    int c = integer;
    for(a=1; a>0; a++){
      for(c = integer; c > 0; c--){
        System.out.print(" ");
        System.out.print(c);
      }
      integer--;
        System.out.println(" ");
      if(integer == 0){
        break;
      }
    }
    
  }
}