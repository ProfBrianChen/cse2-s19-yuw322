import java.util.Scanner;
import java.util.Arrays;
public class lab10{
  public static void main(String[] arg){
    
   
    Scanner userInput = new Scanner(System.in);
    //generating a random array with random number. store them for future use.
    int rdmNum1=(int)(Math.random()*5+1), rdmNum2=(int)(Math.random()*5+1);
    System.out.println("Generating a matrix with width "+rdmNum2+" and height "+rdmNum1+":");
    //randomly decide whether its row major or column major.
    boolean rdmBoolean1 = (rdmNum1%2==0);
    //get the matrix and print it out.
    int[][] tempArray1 = increasingMatrix(rdmNum1,rdmNum2, rdmBoolean1);
    printMatrix(tempArray1, rdmBoolean1);
    
    //the second matrix that has the only difference of major.
    boolean rdmBoolean2 = rdmBoolean1==false;
    
    //generate a similar matrix and print it out.
    System.out.println("Generating a matrix with width "+rdmNum2+" and height "+rdmNum1+":");
    int[][] tempArray2 = increasingMatrix(rdmNum1,rdmNum2, rdmBoolean2);
    printMatrix(tempArray2, rdmBoolean2);
    
    //the second set of matrix
    int rdmNum3=(int)(Math.random()*5+1), rdmNum4=(int)(Math.random()*5+1);
    System.out.println("Generating a matrix with width "+rdmNum4+" and height "+rdmNum3+":");
    boolean rdmBoolean3 = (rdmNum3%2==0);
    int[][] tempArray3 = increasingMatrix(rdmNum3,rdmNum4, rdmBoolean3);
    printMatrix(tempArray3, rdmBoolean3);
     
    //adding the 1st and 2nd matrices up
    System.out.println("Adding two matrices.");
    printMatrix(tempArray1, rdmBoolean1);
    
    System.out.println("plus");
    printMatrix(tempArray2, rdmBoolean2);
    
    System.out.println("output:");
    int[][] tempArray4 = addMatrix(tempArray1,rdmBoolean1,tempArray2,rdmBoolean2);
    printMatrix(tempArray4, rdmBoolean3);
    
    //trying to add them up. If it fails, do nothing and finish the program.
    int[][] tempArray5 = addMatrix(tempArray1,rdmBoolean1,tempArray3,rdmBoolean3);
    if(tempArray5!=null){
    if(tempArray5.length>=1){
      System.out.println("Adding two matrices.");
      printMatrix(tempArray1, rdmBoolean1);

      System.out.println("plus");
      printMatrix(tempArray3, rdmBoolean3);

      System.out.println("output:");      
      printMatrix(tempArray5, rdmBoolean3);
    }
    }
  }
  
 public static int[][] increasingMatrix(int a, int b, boolean c){
   int[][] outputArray = new int[a][b];
   int tempValue = 1;
   if(c){
     for(int d=0; d<a; d++){
       for(int e=0; e<b; e++){
         outputArray[d][e] = tempValue;
         tempValue++;
       }
     }
   }
   else{
     for(int d=0; d<b; d++){
       for(int e=0; e<a; e++){
         outputArray[e][d] = tempValue;
         tempValue++;
       }
   }
   }
  
  return outputArray; 
   
 }
  
 public static void printMatrix(int[][] array, boolean format){
   for(int a=0; a<array.length;a++){
     System.out.print("[ ");
     for(int b=0; b<array[a].length;b++){
       System.out.print(array[a][b]+" ");
     }
     System.out.println("]");
   }
 }
  
  public static int[][] translate(int[][] array){
    int[] linearArray = new int[array.length*array[0].length];
   
    for(int a=0; a<array[0].length; a++){
      for(int b=0; b<array.length; b++){
        linearArray[b+a*array.length] = array[b][a];
      }
    }
    
    int[][] rowArray = new int[array.length][array[0].length];
    
    for(int a=0; a<array.length; a++){
      for(int b=0; b<array[0].length; b++){
        rowArray[a][b] = linearArray[b+array[0].length*a];
      }
    }
    
    return rowArray;
  }
  
  public static int[][] addMatrix(int[][] a, boolean formatA, int[][] b, boolean formatB){
    
    if(a[0].length!=b[0].length || a.length!=b.length){
      //in case their sizes are different, declaring that they cannot be added up and return null
      System.out.println("The arrays cannot be added!");
        return null;
    }
    if(!formatA){
      a = translate(a);
      System.out.println("Translating column makor to row major input. ");
    }
    if(!formatB){
      b = translate(b);
      System.out.println("Translating column makor to row major input. ");
    }
    
    int[][] addArrays = new int[a.length][a[0].length];
    for(int c=0; c<a.length; c++){
      for(int d=0; d<a[0].length; d++){
        addArrays[c][d] = a[c][d]+b[c][d];
      }
    }
    
    return addArrays;
    
  }
  
}