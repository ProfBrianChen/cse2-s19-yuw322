import java.util.Arrays;
public class lab08{
public static void main(String arg[]){
  int myRandomNum = (int)(Math.random() * 50 + 51);
  System.out.println(myRandomNum);
  int[] myArray = new int[myRandomNum];
  for(int a=0;a<myArray.length;a++){
    myArray[a] = (int)(Math.random() * 100);
  }
  System.out.println("The range is "+getRange(myArray));
  System.out.println("The mean is "+getMean(myArray));
  System.out.println("The getStdDev is "+getStdDev(myArray,getMean(myArray)));
  Arrays.sort(myArray);
  shuffle(myArray);
  }
  
public static int getRange(int[] myArray){
  Arrays.sort(myArray);
  int range = myArray[myArray.length-1]-myArray[0];
  return range;
}
  
public static double getMean(int [] myArray){
  int sum = 0;
  for(int a=0; a<myArray.length;a++){
    sum += myArray[a];
  }
  double mean = sum/myArray.length;
  return mean;
}  
public static double getStdDev(int [] myArray, double c){
  int sum = 0;
  for(int a=0;a<myArray.length;a++){
    sum += myArray[a];
  }
  double StdDev = Math.pow((Math.pow(sum-c,2)/(myArray.length-1)),0.5);
  return StdDev;
}

public static void shuffle(int[] myArray){
  for(int a=0;a<myArray.length;a++){
    int trashNum = (int)(Math.random()*myArray.length);
    int temp = myArray[a];
    myArray[a] = myArray[trashNum];
    myArray[trashNum] = temp;
   System.out.print(myArray[a]+" "); 
  }
}  
  
}