/////////////////////
/////CSE002 HW06/////
/////YUEHAN WANG/////
/////////////////////

import java.util.Scanner;  //import the scanner
public class Network{  //define the class
  public static void main(String arg[]){  //main method of java
    
    Scanner myScanner = new Scanner(System.in);  //define the scanner
    
    System.out.print("Input your desired height: ");   //ask for input
    boolean hasHeight = myScanner.hasNextInt();  //check if user typed a valid value
    int height = 0;
    if (hasHeight){  //check if it's an integer
      height = myScanner.nextInt();  //if it is, set the value
    }
    else
       myScanner.next();  //remove the value inside the scanner
        while (height < 0 || !hasHeight){  //in case the user type a wrong value
          System.out.print("You entered an invalid number. Please try again: ");
          hasHeight = myScanner.hasNextInt();  //ask for input again
         while (!hasHeight){  //loop until the user typed in a valid one         
           System.out.print("Error: please enter an integer: ");
           myScanner.next(); 
           hasHeight = myScanner.hasNextInt();  
         }
         height = myScanner.nextInt();
         if (height >0){  //when the value is valid, get out of the loop
         break;
         }
     } 
    
    
    System.out.print("Input your desired width: ");   //ask for input
    boolean hasWidth = myScanner.hasNextInt();  //check if user typed a valid value
    int width = 0;
    if (hasWidth){  //check if it's an integer
      width = myScanner.nextInt();  //if it is, set the value
    }
    else
       myScanner.next();  //remove the value inside the scanner
        while (width < 0 || !hasWidth){  //in case the user type a wrong value
          System.out.print("You entered an invalid number. Please try again: ");
          hasWidth = myScanner.hasNextInt();  //ask for input again
         while (!hasWidth){  //loop until the user typed in a valid one         
           System.out.print("Error: please enter an integer: ");
           myScanner.next(); 
           hasWidth = myScanner.hasNextInt();  
         }
         width = myScanner.nextInt();
         if (width >0){  //when the value is valid, get out of the loop
         break;
         }
     } 
    
    
    System.out.print("Input square size: ");   //ask for input
    boolean hasSquare = myScanner.hasNextInt();  //check if user typed a valid value
    int square = 0;
    if (hasSquare){  //check if it's an integer
      square = myScanner.nextInt();  //if it is, set the value
    }
    else
       myScanner.next();  //remove the value inside the scanner
        while (square < 0 || !hasSquare){  //in case the user type a wrong value
          System.out.print("You entered an invalid number. Please try again: ");
          hasSquare = myScanner.hasNextInt();  //ask for input again
         while (!hasSquare){  //loop until the user typed in a valid one         
           System.out.print("Error: please enter an integer: ");
           myScanner.next(); 
           hasSquare = myScanner.hasNextInt();  
         }
         square = myScanner.nextInt();
         if (square >0){  //when the value is valid, get out of the loop
         break;
         }
     } 
    
    System.out.print("Input edge length: ");   //ask for input
    boolean hasEdge = myScanner.hasNextInt();  //check if user typed a valid value
    int edge = 0;
    if (hasEdge){  //check if it's an integer
      edge = myScanner.nextInt();  //if it is, set the value
    }
    else
       myScanner.next();  //remove the value inside the scanner
        while (edge < 0 || !hasEdge){  //in case the user type a wrong value
          System.out.print("You entered an invalid number. Please try again: ");
          hasEdge = myScanner.hasNextInt();  //ask for input again
         while (!hasEdge){  //loop until the user typed in a valid one         
           System.out.print("Error: please enter an integer: ");
           myScanner.next(); 
           hasEdge = myScanner.hasNextInt();  
         }
         edge = myScanner.nextInt();
         if (edge >0){  //when the value is valid, get out of the loop
         break;
         }
     } 

//--------------print the pattern--------------------
   
    String printValue = "";  //define the printValue
    int divider1 = width/(square+edge);  //define the integer i'm going to use
    int divider2 = height/(square+edge);
    int line = 1;
    int indicator = 0;
    int squarePipe = square/2;
    
    //=================store and print the value=================
    
    if(square%2 == 0){  //situation when square size is even        
     if(divider2 >=1){  //situation when the height is large enough to loop, otherwise jump to the next statement
       for(int p=0; p<divider2; p++){  //loop the statement according to the remainder of height
         
        for(line=1; line<=(square+edge); line++){  
         if(line==1 || line==square){  //store the top and bottom of the "pipe"          
        for(int a=0; a<divider1; a++){          
      printValue = printValue + "#";
      for(int b=0; b<square-2; b++){  //store the value to printValue accoording the the square length
        printValue = printValue + "-";
      } 
        if(square>1){
      printValue = printValue + "#";
        }   
       if(square/2 == line || square/2 == line-1){  //store the connectors according to the square length
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
           
          }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
          } 
         }
        }
           while(1==1){  //store the value out of the loop
             if(indicator==width%(square+edge)){
               break;
             }
           
            printValue = printValue + "#";
             indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
             for(int k=0; k<square-2; k++){
               printValue = printValue + "-";
               indicator++;
               if(indicator==width%(square+edge)){
               break;
             }
             }
             if(indicator==width%(square+edge)){
               break;
             }
                printValue = printValue + "#";
             indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
         if(square/2 == line || square/2 == line-1){
         for(int l=0; l<edge; l++){
         printValue = printValue + "-";
           indicator++;
           if(indicator==width%(square+edge)){
               break;
            }
          }
            if(indicator==width%(square+edge)){
               break;
             }
        }
          else{
          for(int n=0; n<edge; n++){
            printValue = printValue + " ";
             indicator++;
           if(indicator==width%(square+edge)){
               break;
            }
          }
             if(indicator==width%(square+edge)){
               break;
             }
          }
           }
           }
         
      //literally another situation of the body of the pipe
      
        if(line<square && line !=1 && line != square){
          
          for(int c=0; c<divider1; c++){
            
          printValue = printValue + "|";
            for(int d=0; d<square-2; d++){
              printValue = printValue + " ";
              
            }
          printValue = printValue + "|";
            
         if(square/2 == line || square/2 == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
          }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
          } 
         }
         }
          while(1==1){
             
            if(indicator==width%(square+edge)){
               break;
             }
          printValue = printValue + "|";
          indicator++;
            if(indicator==width%(square+edge)){
               break;
             } 
          for(int m=0; m<square-2; m++){
            printValue = printValue + " ";
            indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
            if(indicator==width%(square+edge)){
               break;
             }
            
          printValue = printValue + "|";
          indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
           if(square/2 == line || square/2 == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
           indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
              if(indicator==width%(square+edge)){
               break;
             }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
            indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
            if(indicator==width%(square+edge)){
               break;
             }
         }
          }
        }
           
           
     //similar situation of the vertical connectors of the pipes      
           
          if(line>square && line<=(square+edge)){
            for(int g=0; g<divider1; g++){
              for(int j=1; j<=(square+edge); j++){
               
              if(j==square/2 || j-1==square/2){
                printValue = printValue + "|";
              }
              else{
                printValue = printValue + " ";
              }
            }
            }
            while(1==1){
              if(indicator==width%(square+edge)){
               break;
             }
            for(int o=1; o<=(square+edge); o++){
              if(o==square/2 || o-1==square/2){
                printValue = printValue + "|";
                indicator++;
                if(indicator==width%(square+edge)){
               break;
             }
              }
              else{
                printValue = printValue + " ";
                indicator++;
                if(indicator==width%(square+edge)){
               break;
             }
              }
          }
               if(indicator==width%(square+edge)){
               break;
             }
            }
          }
            
           System.out.println(printValue);  //printout the first line
           printValue = "";  //reset the values
           indicator = 0;
           //loop with line+1 everytime 
      } 
     }
     }
       //--------------------------------------------------------------------------
       //--------------general value outside the loop, within divider2>=0 ---------
       //------------------the structure is basically the same---------------------
       //--------------------------------------------------------------------------
      
   
        for(line=1; line<=height%(square+edge); line++){ 
         if(line==1 || line==square){
          
        for(int a=0; a<divider1; a++){          
      printValue = printValue + "#";
      for(int b=0; b<square-2; b++){
        printValue = printValue + "-";
      } 
        if(square>1){
      printValue = printValue + "#";
        }   
       if(square/2 == line || square/2 == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
           
          }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
          } 
         }
        }
           while(1==1){
             if(indicator==width%(square+edge)){
               break;
             }
           
            printValue = printValue + "#";
             indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
             for(int k=0; k<square-2; k++){
               printValue = printValue + "-";
               indicator++;
               if(indicator==width%(square+edge)){
               break;
             }
             }
             if(indicator==width%(square+edge)){
               break;
             }
                printValue = printValue + "#";
             indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
         if(square/2 == line || square/2 == line-1){
         for(int l=0; l<edge; l++){
         printValue = printValue + "-";
           indicator++;
           if(indicator==width%(square+edge)){
               break;
            }
          }
            if(indicator==width%(square+edge)){
               break;
             }
        }
          else{
          for(int n=0; n<edge; n++){
            printValue = printValue + " ";
             indicator++;
           if(indicator==width%(square+edge)){
               break;
            }
          }
             if(indicator==width%(square+edge)){
               break;
             }
          }
           }
           }
         
      
      
        if(line<square && line !=1 && line != square){
          
          for(int c=0; c<divider1; c++){
            
          printValue = printValue + "|";
            for(int d=0; d<square-2; d++){
              printValue = printValue + " ";
              
            }
          printValue = printValue + "|";
            
         if(square/2 == line || square/2 == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
          }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
          } 
         }
         }
          while(1==1){
             
            if(indicator==width%(square+edge)){
               break;
             }
          printValue = printValue + "|";
          indicator++;
            if(indicator==width%(square+edge)){
               break;
             } 
          for(int m=0; m<square-2; m++){
            printValue = printValue + " ";
            indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
            if(indicator==width%(square+edge)){
               break;
             }
            
          printValue = printValue + "|";
          indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
           if(square/2 == line || square/2 == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
           indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
              if(indicator==width%(square+edge)){
               break;
             }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
            indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
            if(indicator==width%(square+edge)){
               break;
             }
         }
          }
        }
           
           
           
           
          if(line>square && line<=(square+edge)){
            for(int g=0; g<divider1; g++){
              for(int j=1; j<=(square+edge); j++){
               
              if(j==square/2 || j-1==square/2){
                printValue = printValue + "|";
              }
              else{
                printValue = printValue + " ";
              }
            }
            }
            while(1==1){
              if(indicator==width%(square+edge)){
               break;
             }
            for(int o=0; o<(square+edge); o++){
              if(o==square/2 || o-1==square/2){
                printValue = printValue + "|";
                indicator++;
                if(indicator==width%(square+edge)){
               break;
             }
              }
              else{
                printValue = printValue + " ";
                indicator++;
                if(indicator==width%(square+edge)){
               break;
             }
              }
          }
               if(indicator==width%(square+edge)){
               break;
             }
            }
          }
          
           System.out.println(printValue); 
           printValue = "";    
           indicator = 0;
      }
    }
    
      //----------------------------------------------------------------------------------------
      //---------------another situation when divider2<1, which means square is odd-------------
      //----------------------------------------------------------------------------------------
    
    //basical structures are the same
    else{
           for(int p=0; p<divider2; p++){
        for(line=1; line<=(square+edge); line++){ 
          
         if(line==1 || line==square){
          
        for(int a=0; a<divider1; a++){          
      printValue = printValue + "#";
      for(int b=0; b<square-2; b++){
        printValue = printValue + "-";
      } 
        if(square>1){
      printValue = printValue + "#";
        }   
       if(squarePipe == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
           
          }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
          } 
         }
        }
           while(1==1){
             if(indicator==width%(square+edge)){
               break;
             }
           
            printValue = printValue + "#";
             indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
             for(int k=0; k<square-2; k++){
               printValue = printValue + "-";
               indicator++;
               if(indicator==width%(square+edge)){
               break;
             }
             }
             if(indicator==width%(square+edge)){
               break;
             }
                printValue = printValue + "#";
             indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
         if(squarePipe == line-1){  //the only difference is the position of the connectors
         for(int l=0; l<edge; l++){
         printValue = printValue + "-";
           indicator++;
           if(indicator==width%(square+edge)){
               break;
            }
          }
            if(indicator==width%(square+edge)){
               break;
             }
        }
          else{
          for(int n=0; n<edge; n++){
            printValue = printValue + " ";
             indicator++;
           if(indicator==width%(square+edge)){
               break;
            }
          }
             if(indicator==width%(square+edge)){
               break;
             }
          }
           }
           }
         
      
      
        if(line<square && line !=1 && line != square){
          
          for(int c=0; c<divider1; c++){
            
          printValue = printValue + "|";
            for(int d=0; d<square-2; d++){
              printValue = printValue + " ";
              
            }
          printValue = printValue + "|";
            
         if(squarePipe == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
          }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
          } 
         }
         }
          while(1==1){
             
            if(indicator==width%(square+edge)){
               break;
             }
          printValue = printValue + "|";
          indicator++;
            if(indicator==width%(square+edge)){
               break;
             } 
          for(int m=0; m<square-2; m++){
            printValue = printValue + " ";
            indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
            if(indicator==width%(square+edge)){
               break;
             }
            
          printValue = printValue + "|";
          indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
           if(squarePipe == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
           indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
              if(indicator==width%(square+edge)){
               break;
             }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
            indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
            if(indicator==width%(square+edge)){
               break;
             }
         }
          }
        }
           
           
           
           
          if(line>square && line<=(square+edge)){
            for(int g=0; g<divider1; g++){
              for(int j=1; j<=(square+edge); j++){
               
              if(squarePipe == j-1){
                printValue = printValue + "|";
              }
              else{
                printValue = printValue + " ";
              }
            }
            }
            while(1==1){
              if(indicator==width%(square+edge)){
               break;
             }
            for(int o=1; o<=(square+edge); o++){
              if(squarePipe == o-1){
                printValue = printValue + "|";
                indicator++;
                if(indicator==width%(square+edge)){
               break;
             }
              }
              else{
                printValue = printValue + " ";
                indicator++;
                if(indicator==width%(square+edge)){
               break;
             }
              }
          }
               if(indicator==width%(square+edge)){
               break;
             }
            }
          }
            
           System.out.println(printValue); 
           printValue = "";    
           indicator = 0;
            
      } 
     }
     
       //---------------------------------------------
       //--------------outside the loop---------------
       //---------------------------------------------
      
   
        for(line=1; line<=height%(square+edge); line++){ 
         if(line==1 || line==square){
          
        for(int a=0; a<divider1; a++){          
      printValue = printValue + "#";
      for(int b=0; b<square-2; b++){
        printValue = printValue + "-";
      } 
        if(square>1){
      printValue = printValue + "#";
        }   
       if(squarePipe == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
           
          }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
          } 
         }
        }
           while(1==1){
             if(indicator==width%(square+edge)){
               break;
             }
           
            printValue = printValue + "#";
             indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
             for(int k=0; k<square-2; k++){
               printValue = printValue + "-";
               indicator++;
               if(indicator==width%(square+edge)){
               break;
             }
             }
             if(indicator==width%(square+edge)){
               break;
             }
                printValue = printValue + "#";
             indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
         if(squarePipe == line-1){
         for(int l=0; l<edge; l++){
         printValue = printValue + "-";
           indicator++;
           if(indicator==width%(square+edge)){
               break;
            }
          }
            if(indicator==width%(square+edge)){
               break;
             }
        }
          else{
          for(int n=0; n<edge; n++){
            printValue = printValue + " ";
             indicator++;
           if(indicator==width%(square+edge)){
               break;
            }
          }
             if(indicator==width%(square+edge)){
               break;
             }
          }
           }
           }
         
      
      
        if(line<square && line !=1 && line != square){
          
          for(int c=0; c<divider1; c++){
            
          printValue = printValue + "|";
            for(int d=0; d<square-2; d++){
              printValue = printValue + " ";
              
            }
          printValue = printValue + "|";
            
         if(squarePipe == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
          }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
          } 
         }
         }
          while(1==1){
             
            if(indicator==width%(square+edge)){
               break;
             }
          printValue = printValue + "|";
          indicator++;
            if(indicator==width%(square+edge)){
               break;
             } 
          for(int m=0; m<square-2; m++){
            printValue = printValue + " ";
            indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
            if(indicator==width%(square+edge)){
               break;
             }
            
          printValue = printValue + "|";
          indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
           if(squarePipe == line-1){
         for(int e=0; e<edge; e++){
         printValue = printValue + "-";
           indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
              if(indicator==width%(square+edge)){
               break;
             }
        }
         else{
          for(int f=0; f<edge; f++){
            printValue = printValue + " ";
            indicator++;
             if(indicator==width%(square+edge)){
               break;
             }
          }
            if(indicator==width%(square+edge)){
               break;
             }
         }
          }
        }
           
           
           
           
          if(line>square && line<=(square+edge)){
            for(int g=0; g<divider1; g++){
              for(int j=1; j<=(square+edge); j++){
               
              if(squarePipe == j-1){
                printValue = printValue + "|";
              }
              else{
                printValue = printValue + " ";
              }
            }
            }
            while(1==1){
              if(indicator==width%(square+edge)){
               break;
             }
            for(int o=1; o<=(square+edge); o++){
              if(squarePipe == o-1){
                printValue = printValue + "|";
                indicator++;
                if(indicator==width%(square+edge)){
               break;
             }
              }
              else{
                printValue = printValue + " ";
                indicator++;
                if(indicator==width%(square+edge)){
               break;
             }
              }
          }
               if(indicator==width%(square+edge)){
               break;
             }
            }
          }
            
           System.out.println(printValue); 
           printValue = "";    
           indicator = 0;
      }
    }
    //===================end==================== 
      
   }  //end of main method
}  //end of class