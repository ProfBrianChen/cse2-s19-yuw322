/////CSE02 HW05 /////
/////YUEHAN WANG/////
/////////////////////

import java.util.Scanner;  //import the scanner
public class Hw05{  //define the class
  public static void main (String arg[]){  //main method of java program
    
    Scanner StrValue = new Scanner ( System.in );  //define the scanners
    Scanner NumValue = new Scanner ( System.in );  
    
  //ask for the department name
    System.out.print("Please enter your department name:");   //ask for input
    boolean String1 = StrValue.hasNextLine();  //check if user typed a valid value
    String departName;
    while (!String1){  //in case the user type a wrong value
       System.out.print("You entered an invalid answer. Please try again:");   //ask for correct input  
       StrValue.next();  //remove the value inside the scanner
       String1 = StrValue.hasNextLine();  //ask for input again
      }
    departName = StrValue.nextLine(); //set the value for future need
    
  //ask for the course number 
    System.out.print("Please enter your course number:");   //ask for input
    boolean Number1 = NumValue.hasNextInt();  //check if user typed a valid value
    int courseNum = 0;
    if (Number1){  //check if it's an integer
      courseNum = NumValue.nextInt();  //if it is, set the value
    }
    else
       NumValue.next();  //remove the value inside the scanner
        while (courseNum <=0 || !Number1){  //in case the user type a wrong value
          System.out.print("You entered an invalid answer. Please try again:");
          Number1 = NumValue.hasNextInt();  //ask for input again
         while (!Number1){  //loop until the user typed in a valid one         
           System.out.print("You entered an invalid answer. Please try again:");
           NumValue.next(); 
           Number1 = NumValue.hasNextInt();  
         }
         courseNum = NumValue.nextInt();
         if (courseNum >0){  //when the value is valid, get out of the loop
         break;
         }
     } 

   //ask for the times their classes meet in a week
    System.out.print("Please enter the times your class meets in a week:");   //ask for input
    boolean Number2 = NumValue.hasNextInt();  //check if user typed a valid value
     int meetNum = 0;
    if (Number2){
      meetNum = NumValue.nextInt();
    }
    else
       NumValue.next();  //remove the value inside the scannerNumValue.nextInt();
      while (meetNum <=0 || !Number2 || meetNum >5){  //in case the user type a wrong value
          System.out.print("You entered an invalid answer. Please try again:");
          Number2 = NumValue.hasNextInt();  //ask for input again
         while (!Number2){           
           System.out.print("You entered an invalid answer. Please try again:");
           NumValue.next();  //remove the value inside the scanner
           Number2 = NumValue.hasNextInt();  
         }
         meetNum = NumValue.nextInt();
         if (meetNum >0 && meetNum <=5){  //if it satisfy the factors, leave the loop
         break;
         }
     } 
    
    //ask for the time the class started
    System.out.print("Please enter the time the class started in this form:[hours] [minutes] ");   //ask for input
    boolean Number4 = NumValue.hasNextInt();
    boolean Number5 = NumValue.hasNextInt();
    int hours =-1;
    int minutes =-1;
    if (Number4 && Number5){
    hours = NumValue.nextInt();
    minutes = NumValue.nextInt();
    }
    else
      NumValue.next();  //remove the value inside the scanner
     while (hours <=0 || hours >12 || minutes <0 || hours>60){  //in case the user type a wrong value
          System.out.print("You entered an invalid answer. Please try again:");
          Number4 = NumValue.hasNextInt();  //ask for input again
          Number5 = NumValue.hasNextInt();
         while (!Number4 || !Number5){    //repeat until the numbers are both integer
           System.out.print("You entered an invalid answer. Please try again:");
           NumValue.next();  //remove the value inside the scanner
           Number4 = NumValue.hasNextInt();
           Number5 = NumValue.hasNextInt();
         }
         hours = NumValue.nextInt();
         minutes = NumValue.nextInt();
         if (hours >0 && hours <=12 && minutes >=0 && minutes <60){  //if it satisfy the factors, leave the loop
         break;
         }
     } 
    
    //ask for the professor's name
    System.out.print("Please enter your professor's name:");   //ask for input
    boolean String2 = StrValue.hasNextLine();  //check if user typed a valid value
    boolean String3 = StrValue.hasNextLine();
    String profName;
    while (!String2 || !String3){  //in case the user type a wrong value
       System.out.print("You entered an invalid answer. Please try again:");   //ask for correct input  
       StrValue.next();  //remove the value inside the scanner
       String2 = StrValue.hasNextLine();  //ask for input again
       String3 = StrValue.hasNextLine();
      }
    profName = StrValue.nextLine(); 
    

    //ask for the number of students
    System.out.print("Please enter the number of students:");   //ask for input
    boolean Number3 = NumValue.hasNextInt();  //check if user typed a valid value
     int studentNum = 0;
    if (Number3){
      studentNum = NumValue.nextInt();
    }
    else
       NumValue.next();  //remove the value inside the scannerNumValue.nextInt();
      while (studentNum <=0 || !Number3 || studentNum >5){  //in case the user type a wrong value
          System.out.print("You entered an invalid answer. Please try again:");
          Number3 = NumValue.hasNextInt();  //ask for input again
         while (!Number3){           
           System.out.print("You entered an invalid answer. Please try again:");
           NumValue.next();  //remove the value inside the scanner
           Number3 = NumValue.hasNextInt();  //if the answer is valid, store it
         }
         studentNum = NumValue.nextInt();
         if (studentNum >0 && studentNum <=100){  //if it satisfy the factors, leave the loop
         break;
         }
     }
  }  //end of method
}  //end of class