///// YUEHAN WANG 2/8/2019 /////
/////      CSE02 LAB03     /////
////////////////////////////////
////////////////////////////////

//import the scanner
import java.util.Scanner; 
//define the class
public class check {
  //main method of java program
  public static void main(String arg[]){
    
    Scanner myScanner = new Scanner ( System.in );  //construct the instance of scanner
    
    System.out.print("Enter the original cost of the check in the form xx.xx: ");  //prompt the user for the orignial cost
    double checkCost = myScanner.nextDouble();  //record the orginal cost
    
    //prompt the user for the percentage tip
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):");
    double tipPercent = myScanner.nextDouble();  //record the percentage tip
    
    tipPercent /= 100; //converting the percentage into a decimal value
    
    //prompt the user for the number of people
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();  //record the number of people
    
    double totalCost, costPerPerson  //define the values
     int dollars, dimes, pennies;  //define the values
    
    totalCost = checkCost * (1 + tipPercent);  
    costPerPerson = totalCost / numPeople;
    dollars=(int)costPerPerson;
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;

    System.out.println("Each person in the group owes $" + dollars + '.' +dimes + pennies);
      
  } //end of main method
}  //end of class