import java.util.Arrays;
import java.util.Scanner; //import the utilities

public class Straight{  //main class
  public static void main(String[] arg){   //main method
    
    double straightTimes = 0;
    for(int a=0; a<1000000; a++){
    int[][] myArray = shuffledArray();  //shuffle the array
    int[] myHand = fiveCards(myArray);  //choose the top 5 cards from the shuffled array
    boolean straightOrNot = straight(myHand);  //test if it's a straight
    if(straightOrNot){  //if it is a straight, count it.
      straightTimes++;
    }
    }
    //calculate the total percentage and print it out
    double tempNum = (float) (straightTimes/10000);  //do the simple calculation
    System.out.printf("The possibility is: "+"%.4f%n", tempNum ); //print it out
  }
    
  public static int[][] shuffledArray(){
    //reset the array each time
   int[][] shuffledArray = new int[4][13];
    int value = 0;
    //fill each member array in ascending order
    for(int a=0; a<4; a++){
      for(int b=0; b<13; b++){
        shuffledArray[a][b] = value;
        value++;
      }
    }
    //shuffle the array
    int swapNum = 0, rowIndicator = 0, columnIndicator = 0;
    for(int a=0; a<4; a++){
      for(int b=0; b<13; b++){
           rowIndicator = (int)(Math.random()*4);
           columnIndicator = (int)(Math.random()*13);
           swapNum = shuffledArray[a][b];
           shuffledArray[a][b] = shuffledArray[rowIndicator][columnIndicator];
           shuffledArray[rowIndicator][columnIndicator] = swapNum;
      }
    }
    return shuffledArray;
  }
  
  public static int[] fiveCards(int[][] array){
    //choose the top five cards from the shuffled array
    int[] fiveCards = new int[5];
    for(int a=0; a<5; a++){
      fiveCards[a] = array[0][a];
    }   
     return fiveCards;    
  }
  
  public static int straight(int[] array, int k){
    //in case the k is over the range, return 0, which means an error
    if(k<=0 || k>5){
      System.out.println("An error occured.");
      return 0;
    }
    
     //transfer the number into the real poker number
    int[] transferredArray = new int[5];
    for(int a=0; a<5; a++){
      transferredArray[a] = (array[a]%13)+1;
    }
    
    //sort the array from the smallest to biggest
    int swapNum = 0;
    for(int b=0; b<5; b++){
     for(int a=1; b+a<5; a++){
      if(transferredArray[b+a]<transferredArray[b]){
        swapNum = transferredArray[b];
        transferredArray[b] = transferredArray[b+a];
        transferredArray[b+a] = swapNum;
      }
    }
   }
    return transferredArray[k-1];  //return the kth lowest number
  }
    
  public static boolean straight(int[] array){
    
    //test for whether there is a straight by using straight method
    for(int k=1; k<array.length; k++){
      if(straight(array, k+1)-straight(array, k)!=1){
        // if one of them is false, return false
        return false;
      }
    }
    //otherwise, return true
    return true;
    
  }  
  
   
} //end of class