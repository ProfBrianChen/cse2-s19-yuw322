import java.util.Arrays;
import java.util.Scanner; //import the utilities

public class RobotCity{  //define the class
  public static void main(String[] arg){  //main method
    
    //randomly generate city blocks and print them out
    int[][] cityArray = buildCity();
    display(cityArray);
    
    //assign the value to the invade method and print out the result
    cityArray = invade(cityArray, (int)(Math.random()*5+5));
    System.out.println("invaded city: ");
    display(cityArray);
    
    //loop and print the updated value for 5 times
    for(int a=1; a<=5; a++){  
    cityArray = update(cityArray);
    System.out.println("updated city: "+a+" times.");
    display(cityArray);
   }
  }
  
  public static int[][] buildCity(){
    //randomly assign values between 10 to 15
    int[][] array = new int[(int) (Math.random()*5+10)][(int) (Math.random()*5+10)];
    for(int a=0; a<array.length; a++){
      for(int b=0; b<array[0].length; b++){
        //assign each member of the array with number between 100 to 999
        array[a][b] = (int) (Math.random()*900+100);
      }
    }
    return array;  //return it
  }
    
  public static void display(int[][] array){
    
    for(int a=0; a<array.length;a++){
      System.out.printf("{ ");
      for(int b=0; b<array[0].length; b++){
         long temp = array[a][b];
        System.out.printf("%+6d", temp);
      
      }
      System.out.printf("   }");
      System.out.println(" ");
    }
  }
    
  public static int[][] invade(int[][] array, int k){
    int row = 0, column = 0;
    for(int a=1; a<=k; a++){
      //randomly choose the position where robots invade the city
      row = (int) (Math.random()*(array.length));
      column = (int) (Math.random()*(array[0].length));
      //in case the spot is invaded already, redo it
      if(array[row][column]<0){
        a--;
      }
      else{  //in case it is a new spot, invade it by reverse the number
        array[row][column]=-array[row][column];
      }
    }
    return array;  //return the invaded value
  }
  
  public static int[][] update(int[][] array){
    
    for(int a=0; a<array.length; a++){
      for(int b=array[a].length-1; b>0; b--){
        //if the specific member of array has not been invaded, and there's a robot to its west, let it be invaded
        if(array[a][b]>0 && array[a][b-1]<0){
          array[a][b]=-array[a][b];  //reverse the number
        }
        
      }
    }
    return array;  //return the updated value
  }
  
}