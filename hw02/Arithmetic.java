/////CSE02 YUEHAN WANG/////
/////     2/2/2019    /////
/////       HW02      /////
///////////////////////////
///////////////////////////

public class Arithmetic {
  //main method for java program
  public static void main(String arg[]){
  
  //price of goods in store  
  double chocoPrice = 2.05; //price of one bar of chocolate
  double sushiPrice = 7.19; //price of one bundle of sushi
  double cookiePrice = 3.89; //price of one stack of cookie
 
  //number of goods I bought  
  double numChoco = 3; //number of chocolate bars I bought
  double numSushi = 5; //number of sushi I bought
  double numCookie = 1; //number of cookies I bought
    
  //other variable(s)  
  double paSalesTax = 0.06; //the PA tax rate
    
  //TOTAL costs of each good
  double totalChoco, totalSushi, totalCookie, totalTax; //define the total costs of each good
  totalChoco=chocoPrice*numChoco; //calculating for the total cost of chocolate
  totalSushi=sushiPrice*numSushi;  //calculating for the total cost of sushi
  totalCookie=cookiePrice*numCookie;  //calculating for the total cost of cookies
  
  //extra digits destroyer
  totalChoco = (double) ((int) totalChoco*100)/100;  //rip out the useless digits
  totalSushi = (double) ((int) totalSushi*100)/100;  
  totalCookie = (double) ((int) totalCookie*100)/100;    
//--------------------print out----------------------
  System.out.println("---------------------");  
  System.out.println("The total cost of chocolate is "+
                     totalChoco+
                     ", and the tax charged is "+
                     (totalChoco*paSalesTax));  
  System.out.println("The total cost of sushi is "+
                     (totalSushi)+
                     ", and the tax charged is "+
                     (totalSushi*paSalesTax));  
  System.out.println("The total cost of cookies is "+
                     (totalCookie)+
                     ", and the tax charged is "+
                     (totalCookie*paSalesTax));
  System.out.println("---------------------");  
  System.out.println("The total cost of purchases before tax is "+
                     (totalChoco+totalSushi+totalCookie)); //calculating for total cost
  System.out.println("The total tax charged is "+
                     (totalChoco*paSalesTax+
                      totalSushi*paSalesTax+
                      totalCookie*paSalesTax)); //calculating for the sum of tax for each item
  System.out.println("---------------------");
  System.out.println("The actual amount paid is "+
                     (totalChoco+totalSushi+totalCookie)*(1+paSalesTax));  //the total amount paid
  System.out.println("---------------------");  
    
  } //end of main method 
}  //end of class