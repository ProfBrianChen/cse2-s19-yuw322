/////////////////////////////
///// YUEHAN WANG CSE02 /////
/////     2/9/2019      /////
/////        hw03       ///// 
/////////////////////////////
/////////////////////////////

//import the scanner
import java.util.Scanner;
//define the class
public class Convert{
  //main method of java program
  public static void main(String arg[]){
  
    Scanner myScanner = new Scanner ( System.in );  //construct the instance of scanner
    
    System.out.print("Enter the distance in meters: ");  //display the instruction
    double meters = myScanner.nextDouble();  //record the meters
    double inches = (int) (meters * 39.3700797 * 10000);  //calculating for the inches
    double roundInches = inches / 10000;  //kill the useless digits
    
    //display the result
    System.out.println(meters + " meters is "
                     + roundInches + " inches.");
         
  }  //end of main method 
}  //end of class