/////////////////////////////
///// YUEHAN WANG CSE02 /////
/////     2/9/2019      /////
/////        hw03       ///// 
/////////////////////////////
/////////////////////////////

//import the scanner
import java.util.Scanner;
//define the class
public class BoxVolume{
  //main method of java program
  public static void main(String arg[]){
    
     Scanner myScanner = new Scanner ( System.in );  //construct the instance of scanner
     
     System.out.print("The width side of the box is: ");  //prompt the instruction
     double width = myScanner.nextDouble();  //record the width
     System.out.print("The length of the box is: ");  //prompt the instruction
     double length = myScanner.nextDouble();  //record the length
     System.out.print("The height of the box is: ");  //prompt the instruction
     double height = myScanner.nextDouble();  //record the height
     
     int volume =  (int) (width * length * height);  //calculate the volume
     
     System.out.println("The volume inside the box is: " + volume);  //display the final answer
    
  }  //end of main method
}  //end of class