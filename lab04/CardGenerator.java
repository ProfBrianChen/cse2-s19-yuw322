///// YUEHAN WANG CSE002 /////
/////       2/15/19      /////
//////////////////////////////
//////////////////////////////

public class CardGenerator {  //define the class
  
  public static void main(String arg[]){  //main method of the program
    
    int theNumber = (int)(Math.random() * 52 + 1);  //create a random number from 1 to 52
    
    String numberDef = "";  //define numberDef
    boolean display;  //indicator of number from 1 to 10
    
    switch (theNumber){
      case 11: case 24: case 37: case 50:  //transfer number 11 into Jack
      numberDef = "Jack";
      break;
      case 12: case 25: case 38: case 51:   //transfer number 12 into Queen
      numberDef = "Queen";
      break;
      case 13: case 26: case 39: case 52:   //trnsfer number 13 into King
      numberDef = "King";
      break;
      default:
      display = false;  //other cases when number is not 11, 12 or 13
      break;
    }
    
      String theSuit = "";  //define theSuit
    
      switch (theNumber){
      case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13:
      theSuit = "Diamonds";  //define the suit according to the number
      break;
      case 14: case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26:
      theSuit = "Clubs";  //define the suit according to the number
      theNumber = theNumber - 13;  //adjust the number appropriate
      break;
      case 27: case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35: case 36: case 37: case 38: case 39:
      theSuit = "Hearts";  //define the suit according to the number
      theNumber = theNumber - 26;  //adjust the number appropriate
      break;
      case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49: case 50: case 51: case 52:
      theSuit = "Spades";  //define the suit according to the number
      theNumber = theNumber - 39;  //adjust the number appropriate
      break; 
      }
    
   if (display = false)  //cases when number is not from 1 to 10
    System.out.println("You picked the " + numberDef  //display the result
                      + " of " + theSuit);
   else;  //case when number is from 1 to 10
    System.out.println("You picked the " + theNumber
                      + " of " + theSuit);
    
  }  //end of main method
}  //end of class