import java.util.Scanner; //import the scanner
//define the class
public class StringAnalysis{
  //main method of java
  public static void main(String[] arg){
    
    Scanner theScanner = new Scanner(System.in);  //define the scanner
    int theNum = 0;
    
    //ask for input
    System.out.println("Please enter a string for examination: ");
    String stringValue = theScanner.next();
    
    //ask for a specific number of letters to examine
    System.out.println("Do you want to examine an exact number of letters? Enter Y or y for an integer. ");
    String yesOrNo = theScanner.next();    
   if(yesOrNo.equals("Y")||yesOrNo.equals("y")){
    System.out.println("Please enter the number of letters you want to examine with a integer: ");
    //in case a number is needed, check for its validity
    boolean IntNum = false;
    while(1==1){
    IntNum = theScanner.hasNextInt();
    while(!IntNum){
    System.out.println("Please enter the number of letters you want to examine with a integer: ");
    theScanner.next();
    IntNum = theScanner.hasNextInt();          
    }
    theNum = theScanner.nextInt();
    if(theNum<=0){  //loop again if its not valid
    System.out.println("Please enter an integer larger than 0: ");
    }
    else{  //if its valid, break out of the loop
      break;
    }  
   }
  }
   //using other methods to check for the letters
   boolean stringEvaluate = false;
   if(theNum != 0){  //in case a number is typed in
    stringEvaluate = theString(stringValue, theNum);
    if(stringEvaluate==true){
    System.out.println("The characters you entered in the given range are all letters.");
    } 
    else{
    System.out.println("The characters you entered in the given range are not all letters.");
    } 
   } 
   else{  //in case only a string
     theString(stringValue);
    if(stringEvaluate==true){
    System.out.println("The characters you entered are all letters.");
    } 
    else{
    System.out.println("The characters you entered are not all letters.");
    } 
   } 
    
    
  } //end of main method
  
  //start of string checking methods
public static boolean theString(String a){  //cases when not number is provided
  boolean c = true;
  for(int b=1; b<=a.length();b++){  //check for every part of the string, return false as soon as a no-letter occurs
    c = Character.isLetter(a.charAt(b));
    if(!c){
      return c;
    }
  }
  return c;  //if every part of the string is a letter, return true
}
  
 //almost the same structure with the last one, with a range d provided
public static boolean theString(String a, int d){
  boolean c = true;
  for(int b=1; b<=a.length() && b<=d ;b++){
    c = Character.isLetter(a.charAt(b));
    if(!c){
      return c;
    }
  }
  return c;  
}
 //end of string checking method
  
 }  //end of class
                          