import java.util.Scanner; //import the scanner
//==define the class
public class Area{
  //main method of java
  public static void main(String[] arg){
    
    Scanner theScanner = new Scanner(System.in);  //define the scanner
    System.out.println("Please choose a shape among rectangle, triangle and circle.");
   
    //define the three values
    boolean rectangle = false;
    boolean triangle = false;
    boolean circle = false;
    //ask for input
    String theShape = theScanner.next();
    
    if(theShape.equals("rectangle")){  //in case the user entered rectangle
      rectangle = true;
    }
   else if(theShape.equals("triangle")){  //in case the user entered triangle
      triangle = true;
    }    
   else if(theShape.equals("circle")){  //in case the user entered circle
      circle = true;
    }  
    //if the input is invalid
   else{
     //loop until the user gives a valid value
     while(1==1){
     System.out.println("You entered an invalid shape. Please choose from rectangle, triangle and circle.");
     theShape = theScanner.next();
     if(theShape.equals("rectangle")||theShape.equals("triangle")||theShape.equals("circle")){
       break;
      }  
     }
     if(theShape.equals("rectangle")){  //in case the user entered rectangle
      rectangle = true;
    }
   else if(theShape.equals("triangle")){  //in case the user entered triangle
      triangle = true;
    }    
   else if(theShape.equals("circle")){  //in case the user entered circle
      circle = true;
    }     
   }
   
   double height = 0;
   double width = 0;
   double radius = 0;
   boolean numCheck = false;
  //ask for exact value for each shape
  //using inputCheck to check for double, and then calculate each area
    if(rectangle){    
      System.out.println("Please enter the height (with decimal point): ");
      height = inputCheck();
      System.out.println("Please enter the width (with decimal point): ");
      width = inputCheck();
      System.out.println("The area is " + CalcRectangle(height,width));
      }
     else if(triangle){
      System.out.println("Please enter the height (with decimal point): ");
      height = inputCheck();
      System.out.println("Please enter the width (with decimal point): ");
      width = inputCheck();
      System.out.println("The area is " + CalcTriangle(height,width));   
     }       
     else if(circle){
      System.out.println("Please enter the radius (with decimal point): ");
      radius = inputCheck();
      System.out.println("The area is " + CalcCircle(radius));   
     }
      
    }  //end of main method
  
  //method for checking input
 public static Double inputCheck(){ 
    boolean numCheckInt = false;
    boolean numCheckDouble = false;
    Double a =0.0;
    Scanner theScanner = new Scanner(System.in); 
    numCheckInt = theScanner.hasNextInt();
    numCheckDouble = theScanner.hasNextDouble();
    while(numCheckInt||!numCheckDouble){  //check if the input is a double, otherwise, loop it
      theScanner.next();
      System.out.println("Please enter a valid double: ");
      numCheckInt = theScanner.hasNextInt();
      numCheckDouble = theScanner.hasNextDouble();
    }
    a = theScanner.nextDouble();
    return a;   //return the valid value
  }
  
  //method for rectangle
  public static double CalcRectangle(double height, double width){
    double a = height*width;  //area of rectangle
    return a;
  }
  
  //method for triangle
  public static double CalcTriangle(double height, double width){
    double a = height*width*0.5;  //area of triangle
    return a;
  }
  
  //method for circle
  public static double CalcCircle(double radius){
    double a = 3.14*radius*radius;  //area of circle
    return a;
  }  
  
  }    //end of class  