import java.util.Arrays;  //input the utilities
import java.util.Scanner;
public class ArrayGames{  //define the class
  
  //main method of java
  public static void main(String[] arg){  
    Scanner choice = new Scanner(System.in);  //create a scanner
    System.out.print("Enter 'i' or 'I' for insert method, 's' or 'S' for shorten method."  );  //ask for the input
    String userInput = choice.next(); //record the input
    int[] RdmArray = generate();  //generate a random array
   if(userInput.equals("i")||userInput.equals("I")){  //in case the user chose insert method
     int [] RdmArray2 = generate();  
     int [] insertArray = insert(RdmArray, RdmArray2);   //generate a second array and pass them to insert method
      System.out.print("Input 1: ");  //print out the value using 'print' method
      print(RdmArray);
      System.out.print("     ");
      System.out.print("Input 2: ");
      print(RdmArray2);
      System.out.println(" ");
      System.out.print("Output: ");
      print(insertArray);  
      System.out.println(" ");
    }
    else if(userInput.equals("s")||userInput.equals("S")){  //in case the user chose shorten method
      int RdmInput = (int) (Math.random()*31);
      int [] shortenArray = shorten(RdmArray, RdmInput);  //generate a random integer and pass them to the shorten method
      System.out.print("Input 1: ");  //print out the value using 'print' method
      print(RdmArray);
      System.out.print("     ");
      System.out.print("Input 2: " + RdmInput);
      System.out.println(" ");
      System.out.print("Output: ");
      print(shortenArray);            
      System.out.println(" ");
    }
    else{  //in case the user typed other things
      System.out.println("Invalid input. Please try again.");  //ask the user to run the program again
    }   
    
  }
  
  //random array generator
  public static int[] generate(){
    int[] intArray = new int[(int)(Math.random()*11+10)];  //set the length of array in between 10 and 20
    for(int a=0; a<intArray.length-1; a++){  //randomly assign values to each term
      intArray[a] = (int)(Math.random()*21);
    }
    return intArray;
  }
  
  //print method
  public static void print(int[] printArray){
    System.out.print("{");
    for(int a=0; a<printArray.length-1; a++){  //print out the terms of the inpur array
      System.out.print(printArray[a] + ",");
    }
    System.out.print(printArray[printArray.length-1] + "}");  //finish the curve bracket
  }
  
  //insert method
  public static int[] insert(int[] inputArray1, int[] inputArray2){
    int[] outputArray = new int[inputArray1.length+inputArray2.length];  //create an outputArray that has a length of the sum of the two input arrays
    
    int RdmNum = (int) (Math.random()*(inputArray1.length));  //randomly choose where to break out
    for(int a=0,b=0; a<outputArray.length-1; a++,b++){  
      if(RdmNum == a){  //start to insert the second array when the number fits  
        for(int c=0; c<inputArray2.length-1; c++){  //insert it into the outpu array
          outputArray[b+c] = inputArray2[c];
        }
        a += inputArray2.length;  //start to get input from the first array again after the insertion of the second array is finished
      }
      outputArray[a] = inputArray1[b];
    }
    return outputArray;
   }
  
  //shorten method
  public static int[] shorten(int[] checkArray, int checker){
    //in case the input number is bigger than the length of the input array
    if(checker+1 > checkArray.length){  
    int[] tempArray = new int[checkArray.length];  
      for(int a=0; a<checkArray.length-1; a++){
        tempArray[a] = checkArray[a];
      }
      return tempArray;  //assign and return the same value as input
    }
    
    //in case the input number is valid
    int[] tempArray = new int[checkArray.length-1];
    for(int a=0, b=0; a<checkArray.length-1; a++,b++){  //copy the values from the input array
      if(b==checker){  //skip to assign the value to where the integer indicates
        b++;
      }
      tempArray[a]=checkArray[b];  //keep coping
    }
    return tempArray;
  }
  
} //end of class