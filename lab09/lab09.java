import java.util.Scanner;
import java.util.Arrays;
public class lab09{
  public static void main(String[] arg){
    
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Linear or Binray search? 'L' or 'l' for linear, 'B' or 'b' fir binary:" );
    String userInput = myScanner.next();
    if(userInput.equals("L")||userInput.equals("l")){
     System.out.print("Please choose the size of the array with an integer: ");
     int sizeOfArray = myScanner.nextInt();
     System.out.print("Please enter the term you want to search with an integer: ");
     int searchTerm = myScanner.nextInt();
     int[] myTempArray = new int[sizeOfArray];
     myTempArray = RdmArray1(sizeOfArray);
     System.out.println("The array is: " );
     for(int printArray=0; printArray<myTempArray.length; printArray++){
       if((printArray+1)%5==0 && printArray!=0){
     System.out.println(myTempArray[printArray]);
         continue;
       }
     System.out.print(myTempArray[printArray] + " ");
     }
     System.out.println(" ");
     int index =  LinearSearch(myTempArray, searchTerm);
     if(index==-1){
       System.out.println("No values fit the search term.");
     }
     else{
       System.out.println(index + " results were found.");
     } 
      
    }   
    
    
    
    else if(userInput.equals("B")||userInput.equals("b")){
     System.out.print("Please choose the size of the array with an integer: ");
     int sizeOfArray = myScanner.nextInt();
     System.out.print("Please enter the term you want to search with an integer: ");
     int searchTerm = myScanner.nextInt();
     int[] myTempArray = new int[sizeOfArray];
     myTempArray = RdmArray2(sizeOfArray);
     System.out.println("The array is: " );
     for(int printArray=0; printArray<myTempArray.length; printArray++){
       if((printArray+1)%5==0 && printArray!=0){
     System.out.println(myTempArray[printArray]);
         continue;
       }
     System.out.print(myTempArray[printArray] + " ");
     }
     System.out.println(" ");
     int index =  BinarySearch(myTempArray, searchTerm);
     if(index==-1){
       System.out.println("No values fit the search term.");
     }
     else{
       System.out.println(index + " results were found.");
     }     
    }
    
    
    
    else{
    System.out.println("Invalid input.");
    }
       
    
  }

  public static int[] RdmArray1 (int a){
    int[] myArray = new int[a];
    for(int b=0; b<a; b++){
      myArray[b]=(int) (Math.random()*(a+1));
    }
    return myArray;
  }
  
  public static int LinearSearch (int[] tempArray, int a){
    int counter = 0;
    for(int b=0; b<tempArray.length; b++){
      if(tempArray[b]==a){
        counter++;
 
      }
    }
    if(counter==0){
      return -1;
    }
    return counter;
  }
  
 public static int[] RdmArray2 (int a){
    int[] myArray = new int[a];
    for(int b=0; b<a; b++){
      while(1==1){
      myArray[b]=(int) (Math.random()*100+1);
      if(b==0 || myArray[b]>myArray[b-1]){
        break;
      }
     
     }
    }
    return myArray;
  }
  
 public static int BinarySearch (int[] tempArray, int a){
    int counter = 0;
    int mid = (int)tempArray.length/2;
    int bottom = 0, top = tempArray.length;
    
   while(top>=bottom){
     if(tempArray[mid] == a){
       counter++;
       break;
     }
     else if(tempArray[mid] > a){
     top = mid-1; 
     mid = (top+bottom)/2;  
     }
     else if(tempArray[mid] < a){
     bottom = mid+1;
     mid = (top+bottom)/2;  
     }
   }
   
    if(counter==0){
      return -1;
    }
    return counter;
  }  
  
  
}